﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace ToDo
{
    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection")
        {
        }
        public DbSet<Task> Tasks { get; set; }
    }
    
    class ViewModel : INotifyPropertyChanged 
    {
        public ObservableCollection<Task> Tasks { get; set; }
        public ApplicationContext db;
        public ViewModel()
        { 
            db = new ApplicationContext();
            db.Tasks.Load();
            Tasks = db.Tasks.Local;
        }

        //private SQLiteConnection DB = new SQLiteConnection("Data source = dbtodo.db");        

        private string _Result;

        public string Result
        {
            get 
            { 
                return _Result; 
            }
            set 
            { 
                _Result = value; 
                OnPropertyChanged(nameof(Result)); 
            }
        }

        private Task _SelectedTasks;

        public Task SelectedTasks
        {
            get { return _SelectedTasks; }
            set { _SelectedTasks = value; OnPropertyChanged(nameof(SelectedTasks)); }
        }

        public DateTime CreationDate { get; set; } = DateTime.Now;

        private ICommand _addTasks;
        private ICommand _delTasks;
        private ICommand _changeTasks;
        private ICommand _checkTasks;


        public ICommand AddTasks
        {
            get
            {
                return _addTasks ?? (_addTasks = new RelayCommand<string>(AddCommandExecute, AddCommandCanExecute));
            }
        }
        private void AddCommandExecute(string str)
        {
            Task task = new Task();
            task.Content = str;
            task.Check = false;
            Tasks.Add(task);
            db.SaveChanges();
            Result = string.Empty;
        }
        private bool AddCommandCanExecute(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;
            else
                return true;
            
        }

        public ICommand DelTasks
        {
            get
            {
                return _delTasks ?? (_delTasks = new RelayCommand<Task>(DelCommandExecute, DelCommandCanExecute));
            }
        }

        private void DelCommandExecute(Task task)
        {
            Tasks.Remove(task);
            db.SaveChanges();
        }

        private bool DelCommandCanExecute(Task task)
        {
            return true;
        }

        public ICommand ChangeTasks
        {
            get
            {
                return _changeTasks ?? (_changeTasks = new RelayCommand<Task>(ChangeCommandExecute, ChangeCommandCanExecute));
            }
        }

        private void ChangeCommandExecute(Task task)
        {
            task.Content = Result;
            db.Entry(task).State = EntityState.Modified;
            db.SaveChanges();
            Result = string.Empty;
        }

        private bool ChangeCommandCanExecute(Task task)
        {
            if (string.IsNullOrEmpty(Result))
                return false;
            else
                return true;
        }

        public ICommand CheckTasks
        {
            get
            {
                return _checkTasks ?? (_checkTasks = new RelayCommand<Task>(CheckCommandExecute, CheckCommandCanExecute));
            }
        }
        private void CheckCommandExecute(Task task)
        {
            //db.Entry(task).State = EntityState.Modified;
            db.SaveChanges();
        }
        private bool CheckCommandCanExecute(Task task)
        {
            return true;
        }
        //------------------------------------------------------------------------
        public string Error => throw new NotImplementedException();

        public string this[string columnName] => throw new NotImplementedException();



        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyName == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
